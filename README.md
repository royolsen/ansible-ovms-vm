# Ansible OVMS VM module

Ansible module for creating virtual machines from templates on Oracle VM Server using Oracle VM Manager.

By Roy Olsen (2018) based on original oracle-ovm module by Björn Åhl (2015).


## Features

Create virtual machine from template using friendly names

start, stop or delete existing virtual machine by friendly name

 

IMPORTANT: Deleting a virtual machine also removes associated virtual disk images

 

## Credit

Derived from original work by Björn Åhl.

 

## License

Released under The MIT License.

 

## Requirements

* Guest template must have ovmd

* Must create and specify a clone customizer for the template

 

## Limitations

* Use serial: 1 in playbooks to avoid random troubles caused by parallel API operations

* Must always specify vmDomainType for all operations in order to work around  a known issue with json objects.

* Can only have one virtual machines or template with a given friendly name

 

## Examples

Task to create and start a new machine from specified template and clone customizer:

```
  - name: Create and start vm via ovms_vm module
    delegate_to: localhost
    ovms_vm:
      state: present
      ovmmHostname: "pca01.test.net"
      ovmmPort: "7002"
      user: "admin"
      password: "Welcome1"
      vmName: "testmachine01"
      serverPool: "Rack1_ServerPool"
      vmIpAddress: "192.168.100.100"
      vmNetmask: "255.255.255.0"
      vmGateway: "192.168.100.1"
      vmDnsServer: "8.8.8.8"
      vmRootPassword: "Welcome1"
      memory: "4096"
      memoryLimit: "4096"
      cpuCount: "2"
      cpuCountLimit: "2"
      vmTemplate: "OL7U5_X86_64"
      vmCloneDefinition: "TEST_OL7U5_X86_64"
      description: "Test server created with ansible"
      vmDomainType: "XEN_PVM"
    register: vmcreated
```